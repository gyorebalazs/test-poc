import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

public class BatExecution {

    @Test(enabled = false)
    public void testExecuteBatFile() throws IOException, InterruptedException {
        //To execute a bat file
        Process runWork = Runtime.getRuntime().exec("cmd /c start .\\src\\test\\java\\test.bat");
        try {
            InputStream is = runWork.getInputStream();
            int i;
            while ((i = is.read()) != -1) {
                System.out.print((char) i);
            }
        } catch (IOException ioException) {
            System.out.println(ioException.getMessage());
        }
        runWork.waitFor();
    }
}
