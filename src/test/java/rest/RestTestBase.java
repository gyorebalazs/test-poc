package rest;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeSuite;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RestTestBase {

    protected static String currentDate = new SimpleDateFormat("yyyy_MM_dd").format(Calendar.getInstance().getTime());
    protected static String currentDateTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(Calendar.getInstance().getTime());

    @BeforeSuite
    public void beforeSuite() {
        RestAssured.baseURI = "http://10.168.38.131";
        RestAssured.basePath = "DirXIdentityRestService-My-Company";
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        // RestAssured.authentication = basic("Hungs Oliver", "dirx");
    }
}
