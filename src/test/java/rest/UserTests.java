package rest;

import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;

public class UserTests extends RestTestBase {

    @Test (description = "Gets a user and validates it's data >>>")
    public void testGetUser() {
        given().pathParam("uId", "uid-7f001-6f26d110-11da5aeefcf--7ffd").
                header("Authorization", "Basic SHVuZ3MgT2xpdmllcjpkaXJ4").
                when().get("Users/{uId}").
                then().assertThat().statusCode(HttpStatus.SC_OK).
                body("emails.value", hasItem("Gabriela.Morton@My-Company.com")).
                body("name.givenName", is("Gabriela")).
                body("name.familyName", is("Morton")).
                body("userName", is("morton")).
                body("phoneNumbers.value", hasItem("+49 89 323-42651")).
                body("phoneNumbers.value", hasItem("+49 89 323-58564"));
    }

    @Test (description = "Updates a user's description and check if it was updated")
    public void testUpdateUser() {
        JSONArray operationsArray = new JSONArray();
        JSONObject operations = new JSONObject();
        JSONObject operation = new JSONObject();
        operation.put("op", "replace");
        operation.put("path", "description");
        operation.put("value", currentDateTime);
        operationsArray.add(operation);
        operations.put("Operations", operationsArray);

        // Update the user
        given().pathParam("uId", "uid-7f001-6f26d110-11da5aeefcf--7ffd").
                body(operations).
                contentType(ContentType.JSON).
                header("Authorization", "Basic SHVuZ3MgT2xpdmllcjpkaXJ4").
                when().patch("Users/{uId}").
                then().assertThat().statusCode(HttpStatus.SC_NO_CONTENT);

        // Get the user and check the if the update was successful
        given().pathParam("uId", "uid-7f001-6f26d110-11da5aeefcf--7ffd").
                header("Authorization", "Basic SHVuZ3MgT2xpdmllcjpkaXJ4").
                when().get("Users/{uId}?attributes=description").
                then().assertThat().statusCode(HttpStatus.SC_OK).
                body("description", is(currentDateTime));
    }
}