package ldap;

import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapAuthenticationException;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utilities.DBManagement;
import utilities.ExcelUtilities;
import utilities.LdapManagement;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

public class TestSearch {

    @Test(description = "Searches for a user, validates if the cn of the user contains User1 and user01. " +
            "Also checks the home directory, and if other attributes are present >>> ")
    public void testSearchInLDAP() throws Exception {
        LdapManagement ldap = new LdapManagement();

        // Search for the user
        Entry searchResult = ldap.searchFor("cn=user01,ou=users,dc=example,dc=org", "(cn=user1)");

        // Check if the searchResult has cn attribute with values User1 and user01
        assertTrue(searchResult.get("cn").contains("User1", "user01"), "cn is not correct, should contain 'User1' and 'user0, " + "but contains " + searchResult.get("cn"));

        // Check if the home directory contains /home/user01
        assertTrue(searchResult.get("homeDirectory").contains("/home/user01"), "homeDirectory is not correct," + " should contain '/home/user01, but contains " + searchResult.get("homeDirectory"));

        // Check if the attributes cn, homeDirectory and uid are present in the search result
        assertTrue(searchResult.containsAttribute("cn", "homeDirectory", "uid"));

        // Validates the size of the search result
        assertTrue(searchResult.size() > 2);

        ldap.close();
    }

    @Test(enabled = false)
    public void testGetDataFromDB() throws SQLException {
        DBManagement db = new DBManagement();
        Map user = db.getDataOfUserById(1);
        assertEquals(user.get("FamilyName"), "Gyore", "The family name is not correct");
        assertEquals(user.get("FirstName"), "Balazs", "The first name is not correct");
        assertEquals(user.get("Department"), "QA", "The department is not correct");
        assertEquals(user.get("Email"), "balazs.gyore@mail.com", "The email address is not correct");
    }

    @DataProvider(name = "ldapConnectionHappyPath")
    public Object[][] getLdapConnectionHappyPathTestData() {
        List<Map> dataList = ExcelUtilities.getDataFromXls(".\\Test.xlsx", "Happy path");
        Object dataFeed[][] = new Object[dataList.size()][5];
        for (int i = 0; i < dataList.size(); i++) {
            Map data = dataList.get(i);
            dataFeed[i][0] = data.get("Host");
            dataFeed[i][1] = data.get("Port");
            dataFeed[i][2] = data.get("User");
            dataFeed[i][3] = data.get("Password");
            dataFeed[i][4] = data.get("SSL");
        }
        return dataFeed;
    }

    @Test(dataProvider = "ldapConnectionHappyPath", description = "Tries to connect to an LDAP server >>>")
    public void testLdapConnectionHappyPath(String host, String port, String user, String password, String ssl) throws IOException, LdapException {
        LdapManagement connection = null;
        try {
            connection = new LdapManagement(host,1389, user, password,ssl);
        } catch (LdapAuthenticationException e) {
            e.printStackTrace();
        }
        assertNotNull(connection, "Couldn't get authenticated on host: " + host + ":" + port + " with user: " + user +
                " password: " + password + " ssl: " + ssl);
        connection.close();
    }

    @DataProvider(name = "ldapConnectionCannotConnect")
    public Object[][] getLdapCannotConnectTestData() {
        List<Map> dataList = ExcelUtilities.getDataFromXls(".\\Test.xlsx", "Cannot connect");
        Object dataFeed[][] = new Object[dataList.size()][5];
        for (int i = 0; i < dataList.size(); i++) {
            Map data = dataList.get(i);
            dataFeed[i][0] = data.get("Host");
            dataFeed[i][1] = data.get("Port");
            dataFeed[i][2] = data.get("User");
            dataFeed[i][3] = data.get("Password");
            dataFeed[i][4] = data.get("SSL");
        }
        return dataFeed;
    }

    @Test(dataProvider = "ldapConnectionCannotConnect", expectedExceptions = LdapAuthenticationException.class,
            description = "Tries to connect to an LDAP server with wrong credentials, expects an auth exception >>> ")
    public void ldapCannotConnect(String host, String port, String user, String password, String ssl) throws IOException, LdapException {
        LdapManagement connection = null;
        connection = new LdapManagement(host, 1389, user, password, ssl);

        assertFalse(connection.isAuthenticated(), "Couldn't get authenticated on host: " + host + ":" + port + " with user: " + user +
                " password: " + password + " ssl: " + ssl);
        connection.close();
    }
}
