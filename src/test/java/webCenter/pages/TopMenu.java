package webCenter.pages;

import webCenter.pages.users.CreateUserPage;
import webCenter.pages.users.SelectUserPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TopMenu {
    private WebDriver driver;

    @FindBy(id = "menu_0_title")
    private WebElement homeMenu;

    @FindBy(id = "menu_2_title")
    private WebElement selfServiceMenu;

    @FindBy(id = "menu_3_title")
    private WebElement delegationsMenu;

    @FindBy(id = "menu_4_title")
    private WebElement workListMenu;

    @FindBy(id = "menu_6_title")
    private WebElement usersMenu;

    @FindBy(css = "div[aria-label='Select user S']")
    private WebElement selectUser;

    @FindBy(css = "div[aria-label='Create new user n']")
    private WebElement createNewUser;

    @FindBy(id = "menu_8_title")
    private WebElement rolesMenu;

    @FindBy(id = "menu_11_title")
    private WebElement logout;

    public TopMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CreateUserPage goToCreateNewUser() {
        usersMenu.click();
        createNewUser.click();
        return new CreateUserPage(driver);
    }

    public SelectUserPage goToSelectUser() {
        usersMenu.click();
        selectUser.click();
        return new SelectUserPage(driver);
    }

    public LoginPage logout() {
        logout.click();
        return new LoginPage(driver);
    }
}
