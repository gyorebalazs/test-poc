package webCenter.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "loginForm_cn")
    private WebElement nameInput;

    @FindBy(id = "loginForm_password")
    private WebElement passwordInput;

    @FindBy(id = "loginForm_submit")
    private WebElement loginButton;

    @FindBy(id = "loginForm_forward")
    private WebElement passwordForgottenButton;

    public TopMenu login(String name, String password) {
        nameInput.sendKeys(name);
        passwordInput.sendKeys(password);
        loginButton.click();
        return new TopMenu(driver);
    }
}
