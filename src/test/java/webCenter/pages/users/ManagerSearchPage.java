package webCenter.pages.users;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ManagerSearchPage {
    private final WebDriver driver;

    @FindBy(id = "userLinks_searchPanel_operand_0")
    private WebElement searchMatchingRuleSelector;

    @FindBy(id = "userLinks_searchPanel_filterValue_0")
    private WebElement searchFilter;

    @FindBy(id = "userLinks_searchPanel_searchButton")
    private WebElement searchButton;

    @FindBy(id = "userLinks_R_0_C_0")
    private WebElement firstSearchResult;

    public ManagerSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void searchForExactMatch(String searchedLocation) {
        Select matchingRule = new Select(searchMatchingRuleSelector);
        matchingRule.selectByValue("equals");
        searchFilter.sendKeys(searchedLocation);
        searchButton.click();
        firstSearchResult.click();
    }
}
