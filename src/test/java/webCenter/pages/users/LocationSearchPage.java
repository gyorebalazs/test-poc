package webCenter.pages.users;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class LocationSearchPage {
    private final WebDriver driver;

    @FindBy(id = "locationLinks_searchPanel_operand_0")
    private WebElement searchMatchingRuleSelector;

    @FindBy(id = "locationLinks_searchPanel_filterValue_0")
    private WebElement searchFilter;

    @FindBy(id = "locationLinks_searchPanel_searchButton")
    private WebElement searchButton;

    @FindBy(id = "locationLinks_R_0_C_0")
    private WebElement firstSearchResult;

    public LocationSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void searchForExactMatch(String searchedLocation) {
        Select matchingRule = new Select(searchMatchingRuleSelector);
        matchingRule.selectByValue("equals");
        searchFilter.sendKeys(searchedLocation);
        searchButton.click();
        firstSearchResult.click();
    }
}
