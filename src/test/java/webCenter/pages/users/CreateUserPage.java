package webCenter.pages.users;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CreateUserPage {
    private final WebDriver driver;

    @FindBy(xpath = "//span[normalize-space()='Create a user stepwise without approval']")
    private WebElement createAUserStepwiseWithout;

    @FindBy(id = "enterAttributesActivityForm_title")
    private WebElement titleInput;

    @FindBy(id = "enterAttributesActivityForm_sn")
    private WebElement lastNameInput;

    @FindBy(id = "enterAttributesActivityForm_givenname")
    private WebElement firstNameInput;

    @FindBy(id = "enterAttributesActivityForm_description")
    private WebElement descriptionInput;

    @FindBy(id = "enterAttributesActivityForm_dxrstartdate")
    private WebElement startDateInput;

    @FindBy(id = "enterAttributesActivityForm_dxrenddate")
    private WebElement endDateInput;

    @FindBy(id = "enterAttributesActivityForm_employeetype")
    private WebElement employeeTypeSelector;

    @FindBy(id = "enterAttributesActivityForm_employeenumber")
    private WebElement employeeNumberInput;

    @FindBy(id = "enterAttributesActivityForm_dxrlocationlink")
    private WebElement locationInput;

    @FindBy(id = "enterAttributesActivityForm_dxrlocationlink_select_img")
    private WebElement selectLocationLink;

    @FindBy(id = "enterAttributesActivityForm_manager")
    private WebElement managerInput;

    @FindBy(id = "enterAttributesActivityForm_manager_select_img")
    private WebElement selectManagerLink;

    @FindBy(id = "enterAttributesActivityForm_dxrorganizationlink")
    private WebElement companyInput;

    @FindBy(id = "enterAttributesActivityForm_dxrorganizationlink_select_img")
    private WebElement selectCompanyLink;

    @FindBy(id = "enterAttributesActivityForm_dxroulink")
    private WebElement organizationalUnitInput;

    @FindBy(id = "enterAttributesActivityForm_dxroulink_select_img")
    private WebElement selectOrganizationalUnitLink;

    @FindBy(id = "enterAttributesActivityForm_telephonenumber")
    private WebElement phoneInput;

    @FindBy(id = "enterAttributesActivityForm_mobile")
    private WebElement mobileInput;

    @FindBy(id = "enterAttributesActivityForm_mail_add")
    private WebElement emailClickFirst;

    @FindBy(id = "enterAttributesActivityForm_mail_tbody0_edit")
    private WebElement emailInput;

    @FindBy(id = "enterAttributesActivityForm_enter")
    private WebElement saveButton;

    private LocationSearchPage locationSearchPage;
    private CompanySearchPage companySearchPage;
    private OrganisationalUnitSearchPage organisationalUnitSearchPage;
    private ManagerSearchPage managerSearchPage;

    public CreateUserPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CreateUserPage startUserCreationWithoutApproval() {
        createAUserStepwiseWithout.click();
        return this;
    }

    public RequestPrivilegesPage save() {
        saveButton.click();
        return new RequestPrivilegesPage(driver);
    }

    public CreateUserPage setMandatoryFields(String lastName, String firstName, String employeeType, String employeeNumber, String location,
                                             String manager, String company, String organizationalUnit, String email) {
        setLastName(lastName);
        setFirstName(firstName);
        setEmployeeType(employeeType);
        setEmployeeNumber(employeeNumber);
        setLocation(location);
        setManager(manager);
        setCompany(company);
        setOrganizationalUnit(organizationalUnit);
        setEmail(email);
        return this;
    }

    public CreateUserPage and() {
        return this;
    }

    private CreateUserPage setTitle(String title) {
        titleInput.sendKeys(title);
        return this;
    }

    public CreateUserPage setDescription(String description) {
        descriptionInput.clear();
        descriptionInput.sendKeys(description);
        return this;
    }

    public CreateUserPage setStartDate(String startDate) {
        startDateInput.sendKeys(startDate);
        return this;
    }

    public CreateUserPage setEndDate(String endDate) {
        endDateInput.sendKeys(endDate);
        return this;
    }

    public CreateUserPage setPhone(String phone) {
        phoneInput.sendKeys(phone);
        return this;
    }

    public CreateUserPage setMobile(String mobile) {
        mobileInput.sendKeys(mobile);
        return this;
    }

    private void setLastName(String lastName) {
        lastNameInput.sendKeys(lastName);
    }

    private void setFirstName(String firstName) {
        firstNameInput.sendKeys(firstName);
    }

    private void setEmployeeType(String employeeType) {
        Select selector = new Select(employeeTypeSelector);
        selector.selectByValue(employeeType);
    }

    private void setEmployeeNumber(String employeeNumber) {
        employeeNumberInput.sendKeys(employeeNumber);
    }

    private void setLocation(String location) {
        openLocationSearchPage();
        locationSearchPage.searchForExactMatch(location);
    }

    private void setManager(String manager) {
        openManagerSearchPage();
        managerSearchPage.searchForExactMatch(manager);
    }

    private void setCompany(String company) {
        openCompanySearchPage();
        companySearchPage.searchForExactMatch(company);
    }

    private void setOrganizationalUnit(String organizationalUnit) {
        openOrganizationalUnitSearchPage();
        organisationalUnitSearchPage.searchForExactMatch(organizationalUnit);
    }

    private void setEmail(String email) {
        emailClickFirst.click();
        emailInput.sendKeys(email);
    }

    private void openLocationSearchPage() {
        selectLocationLink.click();
        locationSearchPage = new LocationSearchPage(driver);
    }

    private void openManagerSearchPage() {
        selectManagerLink.click();
        managerSearchPage = new ManagerSearchPage(driver);
    }

    private void openCompanySearchPage() {
        selectCompanyLink.click();
        companySearchPage = new CompanySearchPage(driver);
    }

    private void openOrganizationalUnitSearchPage() {
        selectOrganizationalUnitLink.click();
        organisationalUnitSearchPage = new OrganisationalUnitSearchPage(driver);
    }
}
