package webCenter.pages.users;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RequestPrivilegesPage {
    private final WebDriver driver;

    @FindBy(id="availRolesSearch_searchButton")
    private WebElement searchButton;

    @FindBy(id="availableRoles_R_0_C__select_edit")
    private WebElement firstRoleCheckbox;

    @FindBy(id="assignSelection_assignRoles_button1")
    private WebElement assignSelectedPrivileges;

    @FindBy(id="assignRolesForm_enterConfirm")
    private WebElement saveButton;

    public RequestPrivilegesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public RequestPrivilegesPage and() {
        return this;
    }

    public RequestPrivilegesPage selectFirstRole() {
        searchButton.click();
        firstRoleCheckbox.click();
        return this;
    }

    public RequestPrivilegesPage assignSelectedPrivileges() {
        assignSelectedPrivileges.click();
        return this;
    }

    public void save() {
        saveButton.click();
    }
}
