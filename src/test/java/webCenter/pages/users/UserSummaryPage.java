package webCenter.pages.users;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserSummaryPage {
    private final WebDriver driver;

    @FindBy(id="overviewUserForm__displayName")
    private WebElement name;

    @FindBy(id="overviewUserForm_dxrOULink")
    private WebElement organizationalUnit;

    @FindBy(id="overviewUserForm_telephoneNumber")
    private WebElement phone;

    @FindBy(id="overviewUserForm_description")
    private WebElement description;

    @FindBy(id="overviewUserForm_employeeType")
    private WebElement employeeType;

    @FindBy(id="overviewUserForm_employeeNumber")
    private WebElement employeeNumber;

    @FindBy(id="overviewUserForm_manager")
    private WebElement manager;

    @FindBy(id="overviewUserForm_c")
    private WebElement country;

    @FindBy(id="overviewUserForm_dxrLocationLink")
    private WebElement location;

    @FindBy(id="overviewUserForm_mobile")
    private WebElement mobile;

    @FindBy(id="overviewUserForm_facsimileTelephoneNumber")
    private WebElement fax;

    @FindBy(id="overviewUserForm_preferredLanguage")
    private WebElement preferredLanguage;

    @FindBy(id="overviewUserForm_postalAddress")
    private WebElement postalAddress;

    @FindBy(id="overviewUserForm_mail_tbody0_txt")
    private WebElement email;

    @FindBy(id="overviewUserForm_dxrStartDate")
    private WebElement startDate;

    @FindBy(id="overviewUserForm_dxrEndDate")
    private WebElement endDate;

    @FindBy(id="overviewUserForm_dxrPwdPolicyLink")
    private WebElement passwordPolicy;

    @FindBy(id="overviewUserForm_dxrDisableStartDate")
    private WebElement deactivationStartDate;

    @FindBy(id="overviewUserForm_dxrDisableEndDate")
    private WebElement deactivationEndDate;

    @FindBy(id="overviewUserForm_dxrState")
    private WebElement state;

    @FindBy(id="overviewUserForm_dxrDeleteDate")
    private WebElement deleteDate;

    @FindBy(id="overviewUserForm_dxrOrganizationLink")
    private WebElement company;

    @FindBy(id="overviewUserForm_dxrCostUnitLink")
    private WebElement costUnit;

    @FindBy(id="overviewUserForm_dxrContextLink_tbody0")
    private WebElement context;

    @FindBy(id="overviewUserForm_dxrProject_tbody0")
    private WebElement project;

    public UserSummaryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getName() {
        return name.getAttribute("value");
    }

    public String getOrganizationalUnit() {
        return organizationalUnit.getAttribute("value");
    }

    public String getPhone() {
        return phone.getAttribute("value");
    }

    public String getDescription() {
        return description.getAttribute("value");
    }

    public String getEmployeeType() {
        return employeeType.getAttribute("value");
    }

    public String getEmployeeNumber() {
        return employeeNumber.getAttribute("value");
    }

    public String getManager() {
        return manager.getAttribute("value");
    }

    public String getCountry() {
        return country.getAttribute("value");
    }

    public String getLocation() {
        return location.getAttribute("value");
    }

    public String getMobile() {
        return mobile.getAttribute("value");
    }

    public String getFax() {
        return fax.getAttribute("value");
    }

    public String getPreferredLanguage() {
        return preferredLanguage.getAttribute("value");
    }

    public String getPostalAddress() {
        return postalAddress.getAttribute("value");
    }

    public String getEmail() {
        return email.getText();
    }

    public String getStartDate() {
        return startDate.getAttribute("value");
    }

    public String getEndDate() {
        return endDate.getAttribute("value");
    }

    public String getPasswordPolicy() {
        return passwordPolicy.getAttribute("value");
    }

    public String getDeactivationStartDate() {
        return deactivationStartDate.getAttribute("value");
    }

    public String getState() {
        return state.getAttribute("value");
    }

    public String getDeleteDate() {
        return deleteDate.getAttribute("value");
    }

    public String getCompany() {
        return company.getAttribute("value");
    }

    public String getCostUnit() {
        return costUnit.getAttribute("value");
    }

    public String getContext() {
        return context.getAttribute("value");
    }

    public String getProject() {
        return project.getAttribute("value");
    }
}
