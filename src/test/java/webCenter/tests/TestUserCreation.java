package webCenter.tests;

import webCenter.pages.users.CreateUserPage;
import webCenter.pages.users.RequestPrivilegesPage;
import webCenter.pages.users.SelectUserPage;
import webCenter.pages.users.UserSummaryPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;


public class TestUserCreation extends WebCenterTestBase {

    private static CreateUserPage createUserPage;
    private static RequestPrivilegesPage requestPrivilegesPage;
    private static SelectUserPage selectUserPage;
    private static UserSummaryPage userSummaryPage;

    private String lastName = "Test";
    private String firstName = String.valueOf(currentUnixTime);

    @BeforeClass
    public void classSetup() {
        topMenu = loginPage.login("Hungs Olivier", "dirx");
    }

    @AfterClass
    public void classTearDown() {
        topMenu.logout();
        driver.quit();
    }

    @Test(priority = 1, description = "Creates a user >>> ")
    public void testCreateUser() {
        createUserPage = topMenu.goToCreateNewUser();
        requestPrivilegesPage = createUserPage.startUserCreationWithoutApproval().
                and().setMandatoryFields(lastName, firstName, "Contractor", "123456",
                        "My-Company Berlin", "Abele Marc", "Mercato Aurum", "Finances", "balazs@mail.com").
                and().setMobile("12345678").
                and().setStartDate("04/18/2022").
                and().save();
        requestPrivilegesPage.selectFirstRole().and().assignSelectedPrivileges().and().save();
    }

    @Test(priority = 2, dependsOnMethods = "testCreateUser", description = "Checks if the user was created with the given data >>> ")
    public void testCheckCreatedUser() {
        SoftAssert sa = new SoftAssert();
        selectUserPage = topMenu.goToSelectUser();
        userSummaryPage = selectUserPage.searchForExactMatch(lastName + " " + firstName);
        sa.assertEquals(userSummaryPage.getName(), lastName + " " + firstName, "Name is not correct");
        sa.assertEquals(userSummaryPage.getEmployeeType(), "Contractor", "Employee type is not correct");
        sa.assertEquals(userSummaryPage.getEmployeeNumber(), "123456", "Employee number is not correct");
        sa.assertEquals(userSummaryPage.getLocation(), "My-Company Berlin", "Location is not correct");
        sa.assertEquals(userSummaryPage.getManager(), "Abele Marc", "Manager is not correct");
        sa.assertEquals(userSummaryPage.getCompany(), "Mercato Aurum", "Company is not correct");
        sa.assertEquals(userSummaryPage.getOrganizationalUnit(), "Finances", "Organizational unit is not correct");
        sa.assertEquals(userSummaryPage.getEmail(), "balazs@mail.com", "Email is not correct");
        sa.assertEquals(userSummaryPage.getMobile(), "12345678", "Mobile is not correct");
        sa.assertAll();
    }
}
