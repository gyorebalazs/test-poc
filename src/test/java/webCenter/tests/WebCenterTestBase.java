package webCenter.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import webCenter.pages.LoginPage;
import webCenter.pages.TopMenu;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class WebCenterTestBase {

    public static LoginPage loginPage;
    public static TopMenu topMenu;
    public static WebDriver driver;
    public static WebDriverWait waitDriver;
    protected static long currentUnixTime = Instant.now().getEpochSecond();


    @BeforeSuite
    public void globalSetup() {
        WebDriverManager.edgedriver().setup();
        driver = WebDriverManager.edgedriver().create();
        waitDriver = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Maximize the window browser
        Dimension dimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(dimension);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        // Go to initial page
        driver.get("http://10.168.38.131/webCenter-My-Company/");
        loginPage = new LoginPage(driver);
    }

    @AfterSuite
    public void globalTearDown() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getCurrentDateIsoFormatPlus(int years, int months, int days) {
        LocalDate today = LocalDate.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return dateTimeFormatter.format(today.plusYears(years).plusMonths(months).plusDays(days));
    }
}
