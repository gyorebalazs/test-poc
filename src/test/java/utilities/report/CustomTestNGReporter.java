package utilities.report;

import lombok.extern.slf4j.Slf4j;
import org.testng.*;
import org.testng.xml.XmlSuite;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class CustomTestNGReporter implements IReporter {


    //This is the customize emailabel report template file path.
    private static final String emailableReportTemplateFile = "src/test/java/utilities/report/customize-emailable-report-template.html";
    // private String previousTestName = "initial";
    private final List<String> previousTestName = new ArrayList<>();

    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {

        try (FileWriter fw = new FileWriter(("target/surefire-reports/custom-emailable-report.html"))) {
            // Get content data in TestNG report template file.
            String customReportTemplateStr = this.readMailableReportTemplate();

            // Create custom report title.
            String customReportTitle = this.getCustomReportTitle("Custom TestNG Report - " + System.getenv("testenv"));

            // Create test suite summary data.
            String customSuiteSummary = this.getTestSuiteSummary(suites);

            // Create test methods summary data.
            String customTestMethodSummary = this.getTestMethodSummary(suites);

            // Replace report title place holder with custom title.
            customReportTemplateStr = customReportTemplateStr.replaceAll("\\$TestNG_Custom_Report_Title\\$", customReportTitle);

            // Replace test suite place holder with custom test suite summary.
            customReportTemplateStr = customReportTemplateStr.replaceAll("\\$Test_Case_Summary\\$", customSuiteSummary);


            // Replace test methods place holder with custom test method summary.
            customReportTemplateStr = customReportTemplateStr.replace("$Test_Case_Detail$", customTestMethodSummary);

            // Write replaced test report content to custom-emailable-report.html.

            fw.write(customReportTemplateStr);
            fw.flush();
        } catch (Exception ex) {
            log.error("Error during report creation.", ex);
        }
    }

    /* Read template content. */
    private String readMailableReportTemplate() {
        StringBuffer retBuf = new StringBuffer();
        File file = new File(emailableReportTemplateFile);

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {

            String line = br.readLine();
            while (line != null) {
                retBuf.append(line);
                line = br.readLine();
            }

        } catch (IOException ex) {
            log.error("Error reading the report template.", ex);
        }
        return retBuf.toString();
    }

    /* Build custom report title. */
    private String getCustomReportTitle(String title) {
        StringBuffer retBuf = new StringBuffer();
        retBuf.append(title).append(" ").append(this.getDateInStringFormat(new Date()));
        return retBuf.toString();
    }

    /* Build test suite summary data. */
    private String getTestSuiteSummary(List<ISuite> suites) {
        StringBuffer retBuf = new StringBuffer();

        try {
            int totalTestCount = 0;
            int totalTestPassed = 0;
            int totalTestFailed = 0;
            int totalTestSkipped = 0;

            for (ISuite tempSuite : suites) {
                retBuf.append("<tr><td colspan=11><center><b>").append(tempSuite.getName()).append("</b></center></td></tr>");

                Map<String, ISuiteResult> testResults = tempSuite.getResults();

                for (ISuiteResult result : testResults.values()) {

                    retBuf.append("<tr>");

                    ITestContext testObj = result.getTestContext();

                    totalTestPassed = testObj.getPassedTests().getAllMethods().size();
                    totalTestSkipped = testObj.getSkippedTests().getAllMethods().size();
                    totalTestFailed = testObj.getFailedTests().getAllMethods().size();

                    totalTestCount = totalTestPassed + totalTestSkipped + totalTestFailed;

                    /* Test name. */
                    retBuf.append("<td>");
                    retBuf.append(testObj.getName());
                    retBuf.append("</td>");

                    /* Total method count. */
                    retBuf.append("<td>");
                    retBuf.append(totalTestCount);
                    retBuf.append("</td>");

                    /* Passed method count. */
                    retBuf.append("<td bgcolor=green>");
                    retBuf.append(totalTestPassed);
                    retBuf.append("</td>");

                    /* Skipped method count. */
                    retBuf.append("<td bgcolor=yellow>");
                    retBuf.append(totalTestSkipped);
                    retBuf.append("</td>");

                    /* Failed method count. */
                    retBuf.append("<td bgcolor=red>");
                    retBuf.append(totalTestFailed);
                    retBuf.append("</td>");

                    /* Get browser type. */
                    String browserType = tempSuite.getParameter("browserType");
                    if (browserType == null || browserType.trim().length() == 0) {
                        browserType = "";
                    }

                    /* Append browser type. */
                    // retBuf.append("<td>");
                    // retBuf.append(browserType);
                    // retBuf.append("</td>");

                    /* Start Date*/
                    Date startDate = testObj.getStartDate();
                    retBuf.append("<td>");
                    retBuf.append(this.getDateInStringFormat(startDate));
                    retBuf.append("</td>");

                    /* End Date*/
                    Date endDate = testObj.getEndDate();
                    retBuf.append("<td>");
                    retBuf.append(this.getDateInStringFormat(endDate));
                    retBuf.append("</td>");

                    /* Execute Time */
                    long deltaTime = endDate.getTime() - startDate.getTime();
                    String deltaTimeStr = this.convertDeltaTimeToString(deltaTime);
                    retBuf.append("<td>");
                    retBuf.append(deltaTimeStr);
                    retBuf.append("</td>");

                    /* Include groups. */
                    retBuf.append("<td>");
                    retBuf.append(this.stringArrayToString(testObj.getIncludedGroups()));
                    retBuf.append("</td>");

                    /* Exclude groups. */
                    retBuf.append("<td>");
                    retBuf.append(this.stringArrayToString(testObj.getExcludedGroups()));
                    retBuf.append("</td>");
                    retBuf.append("</tr>");
                }
            }
        } catch (Exception ex) {
            log.error("Error generating the report summary.", ex);
        }
        return retBuf.toString();
    }

    /* Get date string format value. */
    private String getDateInStringFormat(Date date) {
        StringBuffer retBuf = new StringBuffer();
        if (date == null) {
            date = new Date();
        }
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        retBuf.append(df.format(date));
        return retBuf.toString();
    }

    /* Convert long type deltaTime to string with format hh:mm:ss. */
    private String convertDeltaTimeToString(long deltaTime) {
        StringBuffer retBuf = new StringBuffer();

        long milli = deltaTime;
        long seconds = deltaTime / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        retBuf.append(seconds);

        return retBuf.toString();
    }

    /* Get test method summary info. */
    private String getTestMethodSummary(List<ISuite> suites) {
        StringBuilder retBuf = new StringBuilder();

        try {
            for (ISuite tempSuite : suites) {
                retBuf.append("<tr><td colspan=7><center><b>" + tempSuite.getName() + "</b></center></td></tr>");

                Map<String, ISuiteResult> testResults = tempSuite.getResults();

                for (ISuiteResult result : testResults.values()) {

                    ITestContext testObj = result.getTestContext();

                    String testName = testObj.getName();

                    /* Get failed test method related data. */
                    IResultMap testFailedResult = testObj.getFailedTests();
                    String failedTestMethodInfo = this.getTestMethodReport(testName, testFailedResult, false, false);
                    retBuf.append(failedTestMethodInfo);

                    /* Get skipped test method related data. */
                    IResultMap testSkippedResult = testObj.getSkippedTests();
                    String skippedTestMethodInfo = this.getTestMethodReport(testName, testSkippedResult, false, true);
                    retBuf.append(skippedTestMethodInfo);

                    /* Get passed test method related data. */
                    IResultMap testPassedResult = testObj.getPassedTests();
                    String passedTestMethodInfo = this.getTestMethodReport(testName, testPassedResult, true, false);
                    retBuf.append(passedTestMethodInfo);
                }
            }
        } catch (Exception ex) {
            log.error("Error getting the test method summary.", ex);
        }
        return retBuf.toString();
    }

    /* Get failed, passed or skipped test methods report. */
    private String getTestMethodReport(String testName, IResultMap testResultMap, boolean passedResult, boolean skippedResult) {
        StringBuilder retStrBuf = new StringBuilder();
        String resultTitle = testName;
        String color;

        if (skippedResult) {
            resultTitle += " - Skipped ";
            color = "yellow";
        } else {
            if (!passedResult) {
                resultTitle += " - Failed ";
                color = "red";
            } else {
                resultTitle += " - Passed ";
                color = "green";
            }
        }

        retStrBuf.append("<tr bgcolor=" + color + "><td colspan=7><center><b>" + resultTitle + "</b></center></td></tr>");

        Set<ITestResult> testResultSet = testResultMap.getAllResults();
        // Sorting result in a list, so they are not randomly placed in the report
        List<ITestResult> listResult = testResultSet.stream().sorted().collect(Collectors.toList());
        for (ITestResult testResult : listResult) {
            if (previousTestName.contains(testResult.getMethod().getMethodName()) && passedResult && testResult.getAttribute("retryCounter") == null) {
                //preventing duplicating passed results and retry of failed tests
            } else {
                String testClassName = "";
                String testMethodName = "";
                String startDateStr = "";
                String executeTimeStr = "";
                String paramStr = "";
                String reporterMessage = "";
                String exceptionMessage = "";
                String testDescription = "";

                //Get testClassName
                testClassName = testResult.getTestClass().getName();

                //Get testMethodName
                testMethodName = testResult.getMethod().getMethodName();

                //Get startDateStr
                long startTimeMillis = testResult.getStartMillis();
                startDateStr = this.getDateInStringFormat(new Date(startTimeMillis));

                //Get Execute time.
                long deltaMillis = testResult.getEndMillis() - testResult.getStartMillis();
                executeTimeStr = this.convertDeltaTimeToString(deltaMillis);

                //Get testDescription
                testDescription = testResult.getMethod().getDescription();

                //Get parameter list.
                Object[] paramObjArr = testResult.getParameters();
                StringBuilder build = new StringBuilder();
                for (Object paramObj : paramObjArr) {
                    try {
                        build.append(paramObj.toString());
                        build.append(" ");
                    } catch (ClassCastException e) {
                        log.error("Error getting the parameter list.", e);
                    }
                    paramStr = build.toString();
                }

                //Get reporter message list.
                List<String> reporterMessageList = Reporter.getOutput(testResult);
                StringBuilder buildReportMessage = new StringBuilder();
                for (String tmpMsg : reporterMessageList) {
                    buildReportMessage.append(tmpMsg);
                    buildReportMessage.append(" ");
                }
                reporterMessage = buildReportMessage.toString();

                //Get exception message.
                Throwable exception = testResult.getThrowable();
                if (exception != null && !skippedResult) {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    exception.printStackTrace(pw);
                    // Only showing the message now to make the result more readable
                    // exceptionMessage = sw.toString();
                    exceptionMessage = " " + testResult.getThrowable().getMessage();
                }

                String whiteBg = "<td bgcolor=\"white\">";
                retStrBuf.append("<tr bgcolor=" + color + ">");

                /* Add test class name. */
                retStrBuf.append("<td>");
                retStrBuf.append(testClassName);
                retStrBuf.append("</td>");

                /* Add test method name. */
                retStrBuf.append("<td>");
                retStrBuf.append(testMethodName);
                retStrBuf.append("</td>");

                //* Add execution time.
                retStrBuf.append(whiteBg);
                retStrBuf.append(executeTimeStr);
                retStrBuf.append("</td>");

/*            //* Add start time.
            retStrBuf.append(whiteBg);
            retStrBuf.append(startDateStr);
            retStrBuf.append("</td>");

            //* Add parameter.
            retStrBuf.append(whiteBg);
            retStrBuf.append(paramStr);
            retStrBuf.append("</td>");

            *//* Add reporter message. *//*
            retStrBuf.append(whiteBg);
            retStrBuf.append(reporterMessage);
            retStrBuf.append("</td>");*/

                /* Add exception message. */
                retStrBuf.append(whiteBg);
                retStrBuf.append(testDescription).append(exceptionMessage);
                retStrBuf.append("</td>");

                retStrBuf.append("</tr>");
            }
            previousTestName.add(testResult.getMethod().getMethodName());
        }
        return retStrBuf.toString();
    }

    /* Convert a string array elements to a string. */
    private String stringArrayToString(String[] strArr) {
        StringBuilder retStrBuf = new StringBuilder();
        if (strArr != null) {
            for (String str : strArr) {
                retStrBuf.append(str);
                retStrBuf.append(" ");
            }
        }
        return retStrBuf.toString();
    }
}
