package utilities;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.SearchCursor;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.*;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.ldap.client.api.NoVerificationTrustManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LdapManagement {

    private LdapConnection connection;

    public LdapManagement() throws LdapException {
        LdapConnectionConfig sslConfig = new LdapConnectionConfig();
        sslConfig.setLdapHost("localhost");
        sslConfig.setUseSsl(false);
        sslConfig.setLdapPort(1389);
        sslConfig.setTrustManagers(new NoVerificationTrustManager());
        sslConfig.setTimeout(500);
        connection = new LdapNetworkConnection(sslConfig);
        connection.bind("cn=admin,dc=example,dc=org", "adminpassword");
    }

    public LdapManagement(String hostName, int port, String userName, String password, String ssl) throws LdapException {
        LdapConnectionConfig sslConfig = new LdapConnectionConfig();
        sslConfig.setLdapHost(hostName);
        sslConfig.setUseSsl(Boolean.parseBoolean(ssl));
        sslConfig.setLdapPort(port);
        sslConfig.setTrustManagers(new NoVerificationTrustManager());
        sslConfig.setTimeout(500);
        connection = new LdapNetworkConnection(sslConfig);
        connection.bind(userName, password);
    }

    public Entry searchFor(String searchBase, String filter) throws LdapException, CursorException {
        // Create the SearchRequest object
        SearchRequest req = new SearchRequestImpl();
        req.setScope(SearchScope.SUBTREE);
        req.addAttributes("*");
        req.setTimeLimit(0);
        req.setBase(new Dn(searchBase));
        req.setFilter(filter);

        // Process the request
        SearchCursor searchCursor = connection.search(req);

        while (searchCursor.next()) {
            Response response = searchCursor.get();

            // process the SearchResultEntry
            if (response instanceof SearchResultEntry) {
                Entry resultEntry = ((SearchResultEntry) response).getEntry();
                return resultEntry;
            }
        }
        return null;
    }

    public String getAttributeOfUser(String userDisplayName, String attributeName) throws LdapException, CursorException {
        // Escaping parentheses for the LDAP search if the name contains it
        if (userDisplayName.contains("(")) {
            userDisplayName = userDisplayName.replace("(", "\\28");
            userDisplayName = userDisplayName.replace(")", "\\29");
        }

        // Create the SearchRequest object
        SearchRequest req = new SearchRequestImpl();
        req.setScope(SearchScope.SUBTREE);
        req.setTimeLimit(3000);
        req.setBase(new Dn("cn=Users,cn=IAM"));
        req.setFilter("(displayname=" + userDisplayName + ")");

        // Process the request
        SearchCursor searchCursor = connection.search(req);

        while (searchCursor.next()) {
            Response response = searchCursor.get();

            // process the SearchResultEntry
            if (response instanceof SearchResultEntry) {
                Entry resultEntry = ((SearchResultEntry) response).getEntry();
                return resultEntry.get(attributeName).get().getString();
            }
        }
        return null;
    }

    public List<Entry> searchForMultipleResults(String searchBase, String filter) throws LdapException, CursorException {
        return searchForMultipleResults(searchBase, filter, 0);
    }

    public List<Entry> searchForMultipleResults(String searchBase, String filter, int resultSizeLimit) throws LdapException, CursorException {
        List<Entry> entries = new ArrayList<>();

        // Create the SearchRequest object
        SearchRequest req = new SearchRequestImpl();
        req.setScope(SearchScope.SUBTREE);
        req.addAttributes("*");
        req.setTimeLimit(3000);
        req.setBase(new Dn(searchBase));
        req.setFilter(filter);
        req.setSizeLimit(resultSizeLimit);

        // Process the request
        SearchCursor searchCursor = connection.search(req);

        while (searchCursor.next()) {
            Response response = searchCursor.get();

            // process the SearchResultEntry
            if (response instanceof SearchResultEntry) {
                Entry resultEntry = ((SearchResultEntry) response).getEntry();
                entries.add(resultEntry);
            }
        }
        return entries;
    }

    public void replaceAnAttribute(String entryDn, String attributeName, String attributeValue) throws LdapException {
        Modification replace = new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, attributeName,
                attributeValue);

        connection.modify(entryDn, replace);
    }

    public boolean isAuthenticated() {
        return connection.isAuthenticated();
    }

    public void close() throws IOException {
        connection.close();
    }
}
