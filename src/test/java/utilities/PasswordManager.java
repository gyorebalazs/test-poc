package utilities;

import de.slackspace.openkeepass.KeePassDatabase;
import de.slackspace.openkeepass.domain.Entry;
import de.slackspace.openkeepass.domain.KeePassFile;

public class PasswordManager {
    KeePassFile database;

    public PasswordManager() {
        database = KeePassDatabase.getInstance("D:\\keepass\\Database.kdbx").openDatabase("");
    }

    public String getPasswordOfUser(String userName) {
        Entry entry = database.getEntryByTitle(userName);
        return entry.getPassword();
    }
}
