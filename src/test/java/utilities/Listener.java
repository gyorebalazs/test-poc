package utilities;

import org.apache.maven.surefire.shared.utils.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Listener implements ITestListener {

/*    @Override
    public void onTestFailure(ITestResult result) {
        log.error("*** Test FAILED *** " + ">>>" + getTestMethodName(result) + "<<<");
        TestBase currentClass = (TestBase) result.getInstance();
        WebDriver driver = currentClass.getDriver();
        try {
            if (driver != null) {
                takeScreenShot(driver, getTestMethodName(result));
            }
        } catch (Exception e) {
            log.error("Cannot take screenshot", e);
        } finally {
            //currentClass.getDriver().quit();
        }
        //reinitialize
        //currentClass.beforeSuite();
    }*/

    private static String getTestMethodName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }

    private void takeScreenShot(WebDriver driver, String testName) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HHmm").format(Calendar.getInstance().getTime());
        FileUtils.copyFile(scrFile, new File("./target/screenshots/" + timeStamp + "_" + testName + ".png"));
    }
}

