package utilities;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ExcelUtilities {
    private static XSSFWorkbook excelWorkbook;

    public static List getDataFromXls(String excelFilepath, String tabName) {
        List<Map> dataList = new ArrayList();
        openExcelFile(excelFilepath);
        XSSFSheet sheetData = openSheetInExcel(tabName);
        for (int aktRow = 1; aktRow <= getLastRowInSheet(sheetData); aktRow++) {
            Map data = new HashMap<>();
            data.put("Host", getCellValue(aktRow, 0, sheetData));
            data.put("Port", getCellValue(aktRow, 1, sheetData));
            data.put("User", getCellValue(aktRow, 2, sheetData));
            data.put("Password", getCellValue(aktRow, 3, sheetData));
            data.put("SSL", getCellValue(aktRow, 4, sheetData));
            dataList.add(data);
        }
        closeExcelFile();
        return dataList;
    }

    private static void openExcelFile(String excelFilepath) {
        try (FileInputStream ExcelFile = new FileInputStream(excelFilepath)) {
            excelWorkbook = new XSSFWorkbook(ExcelFile);
        } catch (Exception e) {
            log.error("Open the given Excel-File " + excelFilepath + " failed because of the error: \n" + e
                    .getMessage());
        }
    }

    private static void closeExcelFile() {
        try {
            if (excelWorkbook != null) {
                excelWorkbook.close();
            }
        } catch (Exception e) {
            log.error("Closing the Excel failed because of Error: \n" + e.getMessage() + "\n");
        }
    }

    private static XSSFSheet openSheetInExcel(String sheetName) {
        try {
            return excelWorkbook.getSheet(sheetName);
        } catch (Exception e) {
            log.error("Open the given Sheet " + sheetName + " failed because of the error: \n" + e.getMessage());
            return null;
        }
    }

    private static int getLastRowInSheet(XSSFSheet sheet) {
        try {
            return sheet.getLastRowNum();
        } catch (Exception e) {
            log.error("The Last Row number of the Sheet " + sheet.getSheetName() + " can't get because of the error: \n" + e.getMessage());
            return -1;
        }
    }

    private static String getCellValue(int row, int column, XSSFSheet sheet) {
        try {
            XSSFCell cell = sheet.getRow(row).getCell(column);
            //If the cell is null is the same as the cell value is empty
            if (cell != null) {
                //If the cell type is a numeric cell than the numeric value must be cast to String value
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    if (DateUtil.isCellDateFormatted(cell)) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy hh:mm a");
                        return dateFormat.format(cell.getDateCellValue());
                    } else {
                        return String.valueOf(cell.getNumericCellValue());
                    }
                } else if (cellType.equals(CellType.BOOLEAN)) {
                    return String.valueOf(cell.getBooleanCellValue());
                } else {
                    return cell.getStringCellValue();
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            log.error("Cell Value of given Cell at Row: " + row + " and Column: " + column + " can't get because of " +
                    "error: \n" + e.getMessage());
            return "";
        }
    }

    public static void writeToXlsCell(String excelFilepath, String sheetName, int row, int column, String data) {
        openExcelFile(excelFilepath);
        XSSFSheet sheetData = openSheetInExcel(sheetName);
        XSSFCell cell = sheetData.getRow(row).getCell(column);
        if (cell == null) {
            XSSFRow currentRow = sheetData.getRow(row);
            cell = currentRow.createCell(column);
        }
        cell.setCellValue(data);
        saveExcelFile(excelFilepath);
        closeExcelFile();
    }

    private static void saveExcelFile(String excelFilepath) {
        try (FileOutputStream out = new FileOutputStream(excelFilepath)) {
            excelWorkbook.write(out);
        } catch (Exception e) {
            log.error("Saving the given Excel-File " + excelFilepath + " failed because of the error: \n" + e
                    .getMessage());
        }
    }
}
