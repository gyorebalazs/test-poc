package utilities;


import java.sql.*;
import java.util.HashMap;
import java.util.Map;


public class DBManagement {

    private Connection conn;

    public DBManagement() {
        openDbConnection();
    }

/*    public Map<String, String> getDataOfUserByIda(int id) throws SQLException {
        Map<String, String> user = new HashMap<>();
        String query = "Select * from [USER] where ID = " + id;
        @Cleanup Statement st = conn.createStatement();
        @Cleanup ResultSet rs = st.executeQuery(query);
        if (rs.next()) {
            user.put("FirstName", rs.getString("IRST_NAME"));
            user.put("FamilyName", rs.getString("FAMILY_NAME"));
            user.put("Email", rs.getString("EMAIL"));
            user.put("Department", rs.getString("DEPARTMENT"));
        }
        closeDbConnection();
        return user;
    }*/

    public Map<String, String> getDataOfUserById(int id) throws SQLException {
        Map<String, String> user = new HashMap<>();
        String query = "Select * from [USER] where ID = " + id;
        try (Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(query)) {

            if (rs.next()) {
                user.put("FirstName", rs.getString("FIRST_NAME"));
                user.put("FamilyName", rs.getString("FAMILY_NAME"));
                user.put("Email", rs.getString("EMAIL"));
                user.put("Department", rs.getString("DEPARTMENT"));
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            closeDbConnection();
        }
        return user;
    }

    private void openDbConnection() {
        try {
            if (conn == null || conn.isClosed()) {
                conn = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=Test;encrypt=false", "sa",
                        "StrongPassword12");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    private void closeDbConnection() {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }

    private void closeStatement(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
        }
    }

    private void closeResultSet(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
        }
    }
}
